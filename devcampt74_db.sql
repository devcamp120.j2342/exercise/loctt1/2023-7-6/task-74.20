-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2023 at 05:29 PM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcampt74_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(10) NOT NULL,
  `student_id` int(10) NOT NULL,
  `subject_id` int(10) NOT NULL,
  `grades` int(10) NOT NULL,
  `exam_date` int(20) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `student_id`, `subject_id`, `grades`, `exam_date`, `create_date`, `update_date`) VALUES
(1, 1, 1, 10, 1688830031, 1688830031, 1688830031),
(2, 1, 2, 9, 1688830086, 1688830086, 1688830086),
(3, 1, 3, 9, 1688830086, 1688830086, 1688830086),
(4, 2, 1, 9, 1688830086, 1688830086, 1688830086),
(5, 3, 4, 8, 1688830086, 1688830086, 1688830086);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) NOT NULL,
  `student_code` varchar(40) NOT NULL,
  `user_name` varchar(40) NOT NULL,
  `first_name` varchar(40) NOT NULL,
  `last_name` varchar(40) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `student_code`, `user_name`, `first_name`, `last_name`, `create_date`, `update_date`) VALUES
(1, 'B1910357', 'HuynhQuangDa', 'Da', 'HuynhQuang', 1688829693, 2147483647),
(2, 'B1910358', 'Nguyen Van A', 'A', 'Nguyen Van', 2147483647, 2147483647),
(3, 'B1910359', 'Nguyen Van B', 'B', 'Nguyen Van', 1688829837, 1688829837),
(4, 'B1910360', 'Nguyen Van C', 'C', 'Nguyen Van', 1688829866, 1688829866),
(5, 'B1910361', 'Nguyen Van D', 'D', 'Nguyen Van', 1688829866, 1688829866);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) NOT NULL,
  `subject_code` text NOT NULL,
  `subject_name` text NOT NULL,
  `credit` int(20) NOT NULL,
  `create_date` int(20) NOT NULL,
  `update_date` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_code`, `subject_name`, `credit`, `create_date`, `update_date`) VALUES
(1, 'MATH', 'Math', 10, 2147483647, 2147483647),
(2, 'ART', 'Art', 20, 2147483647, 2147483647),
(3, 'MUSIC', 'Music', 20, 2147483647, 2147483647),
(4, 'HISTORY', 'History', 30, 2147483647, 2147483647),
(5, 'IT', 'it', 10, 2147483647, 2147483647);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `student_index` (`student_code`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subject_index` (`subject_code`) USING HASH;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `grades_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `grades_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
